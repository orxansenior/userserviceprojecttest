using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerService.Repository.Database;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace UserMessage
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        public readonly CustomerContext userdb;
        public GreeterService(ILogger<GreeterService> logger, CustomerContext userdb)
        {
            _logger = logger;
            this.userdb = userdb;
        }

        public override Task<UserData> DoPayment(UserId request, ServerCallContext context)
        {
            var userdata = userdb.Users.FirstOrDefault(a=>a.Id == request.Id);
            if (userdata !=null)
            {
                return Task.FromResult(new UserData
                {
                    Balance = userdata.Balance,
                    Password = userdata.Password,
                    Role = userdata.Role,
                    Username = userdata.Username,
                    Id = userdata.Id
                });
            }
            return Task.FromResult(new UserData
            {
               Balance = 0,
               Password = "",
               Role = "",
               Username = "",
               Id = 0
            });
        }
    }
}
