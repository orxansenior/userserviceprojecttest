﻿using System;
using System.Collections.Generic;
using System.Text;
using UserService.Common;

namespace UserService.Data.Models
{
    public class Payment : BaseEntity
    {
        public int Amount { get; set; }
        public string Action { get; set; }
        public int UserId { get; set; }

    }
}
