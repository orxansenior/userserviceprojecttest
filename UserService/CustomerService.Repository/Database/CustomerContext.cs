﻿using CustomerService.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerService.Repository.Database
{
    public class CustomerContext:DbContext
    {
        public CustomerContext(DbContextOptions<CustomerContext> options) :base(options)
        {}
        public DbSet<User> Users { get; set; }
    }
}
