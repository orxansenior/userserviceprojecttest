﻿using CustomerService.Data.Resources.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerService.Repository.Infrastrucure
{
    public interface ICustomerRepository
    {
        public LoginDTO Login(UserDTO entity);
        public string Register(UserDTO entity);
    }
}
