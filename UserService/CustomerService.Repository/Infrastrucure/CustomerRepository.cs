﻿using AutoMapper;
using CustomerService.Data.Models;
using CustomerService.Data.Resources.DTO;
using CustomerService.Data.Resources.Enums;
using CustomerService.Repository.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using UserService.Common;

namespace CustomerService.Repository.Infrastrucure
{
    public class CustomerRepository : IBaseRepository<User>,ICustomerRepository
    {
        public readonly CustomerContext Db;
        public readonly IMapper mapper;
        public readonly IConfiguration config;
        public CustomerRepository(CustomerContext Db,IMapper mapper, IConfiguration config)
        {
            this.Db = Db;
            this.mapper = mapper;
            this.config = config;
        }
        public void Add(User entity)
        {
            Db.Users.Add(entity);
            Db.SaveChanges();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<User> GetAll()
        {
            return Db.Users.ToList();
        }

        public User GetById(int id)
        {
            return Db.Users.Where(a => a.Id == id).FirstOrDefault();
        }

        public LoginDTO Login(UserDTO entity)
        {
            User user = Db.Users.FirstOrDefault(a => a.Username == entity.Username);
            LoginDTO response = new LoginDTO();
            if (user==null)
            {
                response.Message = "Bele bir user tapilmadi";
                return response;
            }
            if (user.Password == entity.Password && user.Username == entity.Username )
            {
                var claim = new[] {
                    new Claim(ClaimTypes.Name, user.Username),
                    new Claim("UserRole", user.Role)
                };
                var symmetric = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["SecretKey"]));
                var signing = new SigningCredentials(symmetric, SecurityAlgorithms.HmacSha256);
                var token = new JwtSecurityToken(
                    issuer: "hh",
                    audience: "hh",
                    expires: DateTime.Now.AddMinutes(10),
                    claims: claim,
                    signingCredentials: signing
                    );

                user.Token = Guid.NewGuid().ToString();
                Db.SaveChanges();
                response.Token =new JwtSecurityTokenHandler().WriteToken(token).ToString();
                response.Role = user.Role;
                response.RefreshToken = user.Token;
                response.Message = "Xoş geldiniz";
                response.Username = user.Username;
                response.TokenExpirationTime = 600;
                return response;

            }
            response.Message = "Username ve ya password sehfdir";
            return response;

        }

        public string Register(UserDTO entity)
        {
            User user = new User { Username = entity.Username, Password = entity.Password,Role = UserComponent.UserRoles.Customer.ToString() };
            User existeduser = Db.Users.FirstOrDefault(a=>a.Username == entity.Username);
            if (existeduser ==null)
            {
                Db.Users.Add(user);
                Db.SaveChanges();
                return "Ugurlu registrasya";
            }
            return "User artiq movcuddur";
        
        }

        public void Update(User entity)
        {
            Db.Users.Update(entity);
            Db.SaveChanges();
        }
    }
}
