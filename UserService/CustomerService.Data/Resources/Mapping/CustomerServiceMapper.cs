﻿using AutoMapper;
using CustomerService.Data.Models;
using CustomerService.Data.Resources.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerService.Data.Resources.Mapping
{
    public class CustomerServiceMapper:Profile
    {
        public CustomerServiceMapper()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
        }
    }
}
