﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerService.Data.Resources.DTO
{
    public class LoginDTO
    {
        public string Username  { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public string Message { get; set; }
        public string  RefreshToken { get; set; }
        public int TokenExpirationTime { get; set; }
    }
}
