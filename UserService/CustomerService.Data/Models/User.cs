﻿using System;
using System.Collections.Generic;
using System.Text;
using UserService.Common;

namespace CustomerService.Data.Models
{
    public class User :BaseEntity
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public int Balance { get; set; }
    }
}
