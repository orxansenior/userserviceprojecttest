﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserService.Common;
using UserService.Data.Models;
using UserService.Repository.Database;
using UserService.Repository.Infrastructure;
using static UserService.Greeter;

namespace UserService.Controllers.V1
{
    [Route("api/v1/paymentservice")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        public readonly IPaymentRepository service;
        public readonly IBaseRepository<Payment> baseservice;
        public readonly DatabaseContext Db;
        public PaymentController(IPaymentRepository service,IBaseRepository<Payment> baseservice, DatabaseContext Db)
        {
            this.service = service;
            this.baseservice = baseservice;
            this.Db = Db;
        }
        [HttpGet("allpayments")]
        public List<Payment> GetAllPAyments() {
            return baseservice.GetAll();
        }
        [HttpGet("payment/{id}")]
        public Payment GetPaymentById(int id) {
            return baseservice.GetById(id);
        }
        [HttpDelete("delete/{id}")]
        public string DeletePayment(int id) {
            baseservice.Delete(id);
            return "Success";
        }
        public string UpdatePayment(Payment payment) {
            baseservice.Update(payment);
            return "Success";
        }
        [HttpGet("pay/{userid}")]
        public string AddPayment(Payment payment, int userid) {
            service.AddPaymentUser(payment, userid);
            return "Success";
        }
        [HttpPost("dopayment/{userid}")]
        public IActionResult DoActionPayment(int userid,[FromBody]Payment request) {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new GreeterClient(channel);
            var reply = client.DoPayment(new UserId { Id = Convert.ToInt32(userid) });
            var dopayment = Db.Payments.Add(new Payment { Action = "Ödəniş", Amount = request.Amount, CreatedAt = DateTime.Now, UserId = reply.Id }); 
            Db.SaveChanges();
            return Ok($"Ugurlu Emeliyyat {reply.Username}");
        }
       
    }
}
