﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserService.Common
{
    public interface IBaseRepository<T>
    {
        public List<T> GetAll();
        public T GetById(int id);
        public void Add(T entity);
        public void Update(T entity);
        public void Delete(int id);



    }
}
