﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerService.Data.Resources.DTO;
using CustomerService.Repository.Database;
using CustomerService.Repository.Infrastrucure;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static CustomerService.Greeter;

namespace CustomerService.Controllers
{
    [Route("api/v1/customerservice")]
    [ApiController]
    public class ActionController : ControllerBase
    {
        public readonly CustomerContext Db;
        public readonly ICustomerRepository services;
        public ActionController(CustomerContext Db, ICustomerRepository services)
        {
            this.Db = Db;
            this.services = services;
        }
        [HttpGet("dopayment/{userid}")]
        public  IActionResult DoPayment(int userid)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new GreeterClient(channel);
            var reply = client.DoPayment(new UserId { Id = userid });
            return Ok(reply.Username);


        }
        [HttpGet("getuser")]
        public IActionResult GetAllUsers() {
            return Ok(Db.Users.ToList());
        }
        [HttpPost("login")]
        public LoginDTO Login(UserDTO request) {
            LoginDTO loginresponse = services.Login(request);
            return loginresponse;
        }
        [HttpPost("register")]
        public string Register(UserDTO request) {
            string registerResponse = services.Register(request);
            return registerResponse;
        }
    }
}
