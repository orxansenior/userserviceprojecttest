﻿

using System.Collections.Generic;
using UserService.Common;
using UserService.Data.Models;

namespace UserService.Repository.Infrastructure
{
    public interface IPaymentRepository
    {
        public List<Payment> GetPaymentsByUserId(int id);
        public void AddPaymentUser(Payment entity, int UserId);
    }
}
