﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using UserService.Common;
using UserService.Data.Models;
using UserService.Repository.Database;


namespace UserService.Repository.Infrastructure
{
    public class PaymentRepository : IBaseRepository<Payment>,IPaymentRepository
    {
        public readonly DatabaseContext Db;
        public PaymentRepository(DatabaseContext Db)
        {
            this.Db = Db;
        }

        public void Add(Payment entity)
        {
            Db.Payments.Add(entity);
            Db.SaveChanges();
        }

        public void AddPaymentUser(Payment entity, int UserId)
        {
            Db.Payments.Add(new Payment {Action = "Ödəniş olundu",Amount = 500, CreatedAt = DateTime.Now, UserId = UserId });
            Db.SaveChanges();
        }

        public void Delete(int id)
        {
            Payment data = Db.Payments.FirstOrDefault(a => a.Id == id);
            Db.Payments.Remove(data);
            Db.SaveChanges();
        }

        public List<Payment> GetAll()
        {
           return Db.Payments.ToList();
        }

        public Payment GetById(int id)
        {
           return Db.Payments.Where(a=>a.Id == id ).FirstOrDefault();
        }

        public List<Payment> GetPaymentsByUserId(int id)
        {
            return Db.Payments.Where(a => a.UserId == id).ToList();
        }

        public void Update(Payment entity)
        {
            Payment data = Db.Payments.FirstOrDefault(a => a.Id == entity.Id);
            Db.Payments.Update(data);
            Db.SaveChanges();
        }
    }
}
